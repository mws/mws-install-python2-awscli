# mws-install-python2-awscli

This is a reusable Ansible role that installs the AWS command-line tools into an isolated Python environment at `/opt/awscli`.

## Usage

Include the role in your Ansible `roles_path` either by ansible-galaxy (preferred) or git subtree (not recommended) and then add it to your playbooks.
